package epsolution.uk.co.musicplayer;

import android.icu.text.SimpleDateFormat;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private MediaPlayer mMediaPlayer;
    private TextView mLeftTime;
    private TextView mRightTime;
    private SeekBar mSeekBar;

    private Button mPrevBtn;
    private Button mPlayBtn;
    private Button mNextBtn;
    private Thread mThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpUI();
        mSeekBar.setMax(mMediaPlayer.getDuration());

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (fromUser) {
                    mMediaPlayer.seekTo(progress);
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");

                int currentPosition = mMediaPlayer.getCurrentPosition();
                int duration = mMediaPlayer.getDuration();

                mLeftTime.setText(dateFormat.format(new Date(currentPosition)));
                mRightTime.setText(dateFormat.format(new Date(duration - currentPosition)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    public void setUpUI() {

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer = MediaPlayer.create(getApplicationContext(),
                R.raw.bensound_slowmotion);

        ImageView artistImage = (ImageView) findViewById(R.id.imageView);
        mLeftTime = (TextView) findViewById(R.id.leftTime);
        mRightTime = (TextView) findViewById(R.id.rightTime);

        mSeekBar = (SeekBar) findViewById(R.id.seekBar);

        mPrevBtn = (Button) findViewById(R.id.prevBtn);
        mPlayBtn = (Button) findViewById(R.id.playBtn);
        mNextBtn = (Button) findViewById(R.id.nextBtn);

        mPrevBtn.setOnClickListener(this);
        mPlayBtn.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);

        artistImage.setBackgroundResource(R.drawable.slowmotion);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevBtn: {
                backMusic();
                break;
            }
            case R.id.playBtn: {
                if (mMediaPlayer.isPlaying()) {
                    pauseMusic();
                } else {
                    startMusic();
                }

                break;
            }
            case R.id.nextBtn: {
                nextMusic();
                break;
            }

            default: {

                break;
            }
        }
    }


    public void pauseMusic() {
        if (mMediaPlayer != null) {
            mMediaPlayer.pause();
            mPlayBtn.setBackgroundResource(android.R.drawable.ic_media_play);
        }
    }

    public void startMusic() {
        if (mMediaPlayer != null) {
            mMediaPlayer.start();
            updateThread();
            mPlayBtn.setBackgroundResource(android.R.drawable.ic_media_pause);
        }
    }

    public void updateThread() {
        mThread = new Thread() {
            @Override
            public void run() {
                try {

                    while (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                        Thread.sleep(50);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int newPosition = mMediaPlayer.getCurrentPosition();
                                int duration = mMediaPlayer.getDuration();

                                mSeekBar.setMax(duration);
                                mSeekBar.setProgress(newPosition);

                                mLeftTime.setText(String.valueOf(new SimpleDateFormat("mm:ss")
                                        .format(new Date(mMediaPlayer.getCurrentPosition()))));
                                mRightTime.setText(String.valueOf(new SimpleDateFormat("mm:ss")
                                        .format(new Date(mMediaPlayer.getDuration() - mMediaPlayer.getCurrentPosition()))));
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        mThread.start();
    }

    public void backMusic() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.seekTo(0);
        }
    }

    public void nextMusic() {
        if (mMediaPlayer.isPlaying()){
           mMediaPlayer.seekTo(mMediaPlayer.getDuration());
        }
    }


    @Override
    protected void onDestroy() {

        if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }

        mThread.interrupt();
        mThread = null;

        super.onDestroy();
    }


}
